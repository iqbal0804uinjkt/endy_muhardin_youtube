delete from peserta_pelatihan;
delete from peserta;

insert into peserta (id, nama, email, tanggal_lahir)
values ('aa', 'peserta test 001', 'peserta.test.001@gmail.com', '2021-1-2');

insert into peserta (id, nama, email, tanggal_lahir)
values ('ab', 'peserta test 002', 'peserta.test.002@gmail.com', '2021-1-3');

insert into peserta (id, nama, email, tanggal_lahir)
values ('ac', 'peserta test 003', 'peserta.test.003@gmail.com', '2021-1-4');
