package com.iqbal.ambang.pelatihan.entity;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Peserta {

    @Id
    private String id;
    private String nama;
    private String email;
}
